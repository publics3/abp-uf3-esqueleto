## 0.METODOLOGIA DE TRABAJO

- [ ] Cada uno de los apartados se realizará en un script ``.sql`` separado.
- [ ] Los apartados **2, 3 y 10 se realizan en grupo**, es decir, se trabaja de manera conjunta en un único PC y se entrega un única
fichero script ``.sql`` por cada apartado (3 ficheros en total).
- [ ] Los apartados **4 a 9 se realizan en grupo, pero cada miembro del grupo tiene asignado una de las tareas**. Los grupos de 
2 miembros solamente completan 2 de las 3 tareas. Por tanto, hay un ``.sql`` por cada tarea.
- [ ] Cuando todo el código esté subido de cada tarea, se revisará por el professor y se fusionará en master si está correcto.
- [ ] Se trabaja por ``issues`` que se puede ver en el siguiente apartad. Es obligatorio trabajar con ``issues`` y ``Merge Requests``.
- [ ] Es **obligatorio trabajar en remoto para que la Base de Datos** pueda ser consultada por el professor.


## 1.GIT

- [ ] Instalar DataGrip.
- [ ] Clonar todos los miembros del grupo el proyecto creado en GIT en vuestro DataGrip.
- [ ] Trabajar por ``issues`` y ``Merge Request`` para separar el trabajo correctamente.
- [ ] El apartado **1** implica una nueva ``issue`` y, por lo tanto, una nueva ``Merge Request`` con los ficheros ``.dia`` e imagenes.
- [ ] Cada apartado **2, 3 y 10** implica una nueva ``issue`` y, por lo tanto, una nueva ``Merge Request`` y un nuevo fichero ``.sql``.
- [ ] Cada tarea de los apartados **del 4 al 9** implica una nueva ``issue`` y, por lo tanto, una nueva ``Merge Request`` y un nuevo fichero ``.sql`` por cada tarea, es decir, 3 por apartado.
- [ ] Cada vez que se termina una ``issue`` se debe asignar la ``Merge Request`` generada al professor responsable de la corrección:

    * [ ] Si está todo correcto se fusionará con ``main``.
    * [ ] Si hay algún error se pondran comentarios en la ``Merge Request`` y se devolverá al alumno que la tenía assignada previamente para que la corrija.

